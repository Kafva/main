CC?=clang
CFLAGS?=-O2

EXEC=main
DEP_PATH=/home/jonas/Repos/matrix
DEP_A=libmatrix.a
OBJS=obj/main.o obj/calc.o

$(EXEC): obj/main.o obj/calc.o $(DEP_PATH)/$(DEP_A)
	$(CC) $(CFLAGS) -I include -I $(DEP_PATH)/include $^ -o $@

obj/%.o: src/%.c
	@mkdir -p obj
	$(CC) $(CFLAGS) -I include -I $(DEP_PATH)/include $< -c -o $@

run: $(EXEC)
	./$(EXEC)

clean:
	rm -f $(EXEC) obj/* *.o
