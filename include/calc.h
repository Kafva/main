#include "matrix.h"
#define DIM 3
#define SEED 50

/// Generates a matrix with a given seed
int* get_matrix(int seed);
