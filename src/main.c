#include "calc.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]){

	int* m = get_matrix(SEED);

	print_matrix(m,DIM,DIM,"m");

	free(m);

	return 0;
}
