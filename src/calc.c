#include "calc.h"
int* get_matrix(int seed) {
	
	int* m = matrix_init(DIM, DIM, seed);

	for (int i = 1; i < DIM*DIM-1; i++){
		m[i] = m[i-1] + matrix_sum(m,DIM,DIM) ;
		m[i] += get_nearest_even(m[i]);
	}

	return m;
}

